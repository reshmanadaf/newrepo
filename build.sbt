name := "crudapp"
 
version := "1.0" 
      
lazy val `crudapp` = (project in file(".")).enablePlugins(PlayScala)

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"
      
resolvers += "Akka Snapshot Repository" at "http://repo.akka.io/snapshots/"
      
scalaVersion := "2.12.2"



libraryDependencies ++= Seq( jdbc , ehcache , ws , specs2 % Test , guice )
//libraryDependencies += "com.typesafe.play" %% "play-json" % "2.6.9"

libraryDependencies += "com.typesafe.play" %% "play-iteratees-reactive-streams" % "2.6.1"


unmanagedResourceDirectories in Test <+=  baseDirectory ( _ /"target/web/public/test" )


// only for Play 2.6.x (Scala 2.12)

libraryDependencies ++= Seq(
  "org.reactivemongo" %% "play2-reactivemongo" % "0.16.0-play26"
)

routesGenerator := InjectedRoutesGenerator

libraryDependencies += ws

import play.sbt.routes.RoutesKeys

RoutesKeys.routesImport += "play.modules.reactivemongo.PathBindables._"



