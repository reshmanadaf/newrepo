package model

case class Person(

                  id:String,
                 firstname: String,
                 lastname: String,
                 age : Int)
                 //feeds: List[Feed])

/*
case class Feed(
                 name: String)
*/

object JsonFormats {
  import play.api.libs.json.Json

  // Generates Writes and Reads for Feed and User thanks to Json Macros
//  implicit val feedFormat = Json.format[Feed]
  implicit val userFormat = Json.format[Person]

}