package model

case class Photo(
                  photoid:String,
                  photoName:String

                )


object Photo {

  import play.api.libs.json.Json

  implicit val userFormat = Json.format[Photo]


}