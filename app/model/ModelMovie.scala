package  model




case class Movie(
                  _id:String,
                  user:String,
                  movieName:String,
                 yor:Option[Int],
                 roleInMovie:Option[String],
                 charName:Option[String],
                 awards:Option[String],
                 duration: Option[Int],
                 movieInfo:Option[String],
                 movieUrls:Option[Array[String]]
                )


object Movie{

  import play.api.libs.json.Json

  implicit val userFormat = Json.format[Movie]





}


