package controllers

import com.sun.corba.se.impl.orbutil.closure
import javax.inject._
import model.Qualification
import play.api.Logger
import play.api.libs.json.Json
import play.api.mvc._
import play.modules.reactivemongo.{MongoController, ReactiveMongoApi, ReactiveMongoComponents}
import reactivemongo.play.json.collection.JSONCollection

import scala.concurrent.{ExecutionContext, Future}

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class QualController @Inject() (cc: ControllerComponents, val reactiveMongoApi: ReactiveMongoApi)
  extends AbstractController(cc) with MongoController with ReactiveMongoComponents {


  /**
   * Create an Action to render an HTML page with a welcome message.
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */
  /*def index = Action {
    Ok(views.html.index("Your new application is ready."))
  }*/




  /*def add = Action.async(parse.json) {  request =>


    request.body.validate[Qualification].map { qualification =>

      // collection.flatMap(_.insert(qualification)).map { lastError =>
      collection.flatMap(_.insert(qualification)).map { lastError =>
        Logger.debug(s"Successfully inserted with LastError: $lastError")
        Created(Json.obj("error" -> false, "msg" -> "Qualification Created."))
      }

    }.getOrElse(Future(BadRequest("invalid json")))

  }
*/


}
