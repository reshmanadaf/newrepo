package controllers

import java.util.Base64

import javax.inject.{Inject, Singleton}
import play.api.mvc.{AbstractController, ControllerComponents}
import play.modules.reactivemongo.{MongoController, ReactiveMongoApi, ReactiveMongoComponents}
import play.api.libs.json.{JsObject, JsString, JsValue, Json}
import reactivemongo.play.json.collection.JSONCollection
import akka.stream.Materializer
import reactivemongo.play.json.JSONSerializationPack
import java.nio.file.{Files, Paths}

import play.Logger
import play.modules.reactivemongo.MongoController.readFileReads
import reactivemongo.api.gridfs.ReadFile
import reactivemongo.play.json._
import reactivemongo.api.gridfs.Implicits._





@Singleton
class FileController @Inject()(cc: ControllerComponents, val reactiveMongoApi: ReactiveMongoApi,materializer: Materializer)
  extends AbstractController(cc) with MongoController with ReactiveMongoComponents {


  implicit val ec = cc.executionContext

  implicit def m: Materializer = materializer

  val gridFS = reactiveMongoApi.gridFS
  type JSONReadFile = ReadFile[JSONSerializationPack.type, JsString]

  def collection =database.map(_.collection[JSONCollection]("Pictures"))


  val fsParser = gridFSBodyParser(reactiveMongoApi.asyncGridFS)

  def uploadNew = Action(fsParser) { request =>

    val file: ReadFile[JSONSerializationPack.type, JsValue] = request.body.files.head.ref
    Ok(Json.obj("error" -> false, "msg" -> "uploaded", "id" -> file.id))
  }


  def upload = Action(parse.temporaryFile) { request =>

    request.body.moveTo(Paths.get(s"/home/reshma/Downloded"), replace = true)
    Ok(Json.obj("msg" -> "file uploaded successfully"))

  }


  def download(id:String) = Action.async { request =>


    val file = gridFS.find[JsObject,JSONReadFile](Json.obj("id"->id))
      //.collect[List]().map {

      /*val files=files.map { file =>
        file.id->file
      }
    }*/
    Logger.debug("file")
    request.getQueryString("inline") match {

      case Some("true") =>
        serve[JsString, JSONReadFile](gridFS)(file, CONTENT_DISPOSITION_INLINE)

      case _ => serve[JsString, JSONReadFile](gridFS)(file)

   // def cursor = gridFS.find[JsObject,JSONReadFile](Json.obj("id"->id))
    //Cursor.head

    }
  }



  /*def downld(id:String) = Action.async { request =>

  Ok.sendFile(
  content = new java.io.File(s"/home/reshma/ex.js"),
  inline = false
)

   }*/


  //Json transformers are used to build reads object.this object is passed to the transform method which you call on your Json object.
  //It will change the JSON object and give result as success or failure.where result is the new modified JSON.
  /*
  * def jsonXForm(value: String) = (__ \ "customerId").json.update(
(__ \ "customerId").json.put(JsString(value))
)
j     son.transform(jsonXForm(yourNewValue)) match {...}`
  *
  *
  *
  * */

}













