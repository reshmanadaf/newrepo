package controllers


import akka.actor.FSM.Failure
import akka.actor.Status.Success
import javax.inject.{Inject, Singleton}
import model.{Movie, Photo}
import model.Movie._
import play.api.Logger
import play.api.libs.json.{JsObject, JsString, Json}
import reactivemongo.play.json._

import scala.concurrent.{ExecutionContext, Future}
import play.api.mvc.{AbstractController, ControllerComponents}
import play.modules.reactivemongo.{MongoController, ReactiveMongoApi, ReactiveMongoApiComponents, ReactiveMongoComponents}
import reactivemongo.api.{Cursor, ReadPreference}
import reactivemongo.bson.{BSONDocument, BSONObjectID}
import reactivemongo.play.json.collection.JSONCollection



@Singleton
class MovieController @Inject() (cc: ControllerComponents, val reactiveMongoApi: ReactiveMongoApi)
  extends AbstractController(cc) with MongoController with ReactiveMongoComponents {


  implicit def ec: ExecutionContext = cc.executionContext

  def collection: Future[JSONCollection] = database.map(
    _.collection[JSONCollection]("Movie"))


  /*def addMovie() =Action.async(parse.json) { implicit request =>

    val m= request.body.as[JsObject] + ("_id" -> Json.toJson(BSONObjectID.generate().stringify))
      //request.body.validate[Movie].map { movie =>

    m.validate[Movie].map{ movie=>
     /* collection.flatMap(_.insert(movie)).map { lastError =>
        Logger.debug(s"Successfully inserted with LastError: $lastError")
        Created(Json.obj("error" -> false, "msg" -> "Qualification Created."))
      }*/

      val insResult = Movie.addMovie(collection, movie)
      insResult.map { lastError =>
        Logger.debug(s"Successfully inserted with LastError: $lastError")
        Created(Json.obj("error" -> false, "msg" -> "User Created.", "data" -> Json.toJson(lastError)))
      }
    }.getOrElse(Future(BadRequest("Invalid Json")))


  }*/

/*
  def addMovie() = Action.async(parse.json) { implicit request =>

    //request.body.as[JsObject] + ("_id" -> Json.toJson(BSONObjectID.generate.stringify)) + ("source" -> JsString("ekmoka"))
    request.body.validate[Movie].map { movie =>

      collection.flatMap(_.insert(movie)).map { lastError =>
        // Logger.debug(s"Successfully inserted with LastError: $lastError")
        Created(Json.obj("error" -> false, "msg" -> "Movie Created. $lastError"))
      }

    }.getOrElse(Future(BadRequest("invalid json")))
  }*/




  def addMov() = Action.async(parse.json) { implicit request =>
    request.body.as[JsObject] + ("_id" -> Json.toJson(BSONObjectID.generate.stringify)) + ("source" -> JsString("crudapp"))

    //val id= BSONObjectID.generate()
    request.body.validate[Movie].map { movie =>

      collection.flatMap(_.insert(movie)).map { lastError =>
        Logger.debug((s"successfully inserted with LastError: $lastError"))
        Created(s"successfully inserted document with id $BSONObjectID")
      }




      /*  mv.validate[Movie].map { movie =>
      // movie is an object
      collection.flatMap(_.insert(movie)).map { lastError =>
         //Logger.debug(s"Successfully inserted with LastError: $lastError")
        Created(Json.obj("error" -> false, "msg" -> "Movie Created.", "id" -> mv.id))
      }*/
    }.getOrElse(Future.successful(BadRequest(Json.obj("error" -> true, "msg" -> "Invalid JSON"))))

  }




  def updateMovie(id: BSONObjectID) = Action.async(parse.json) { implicit request =>

    // val updateResult = collection.flatMap(_.update(Json.obj("_id" -> id), Json.obj("$set" ->request.body)))
    val updateResult = collection.flatMap(_.update(Json.obj("_id" -> id),Json.obj("$set" -> request.body), upsert = true ))


    updateResult.map { lastError =>
       Logger.debug(s"Successfully Updated with LastError: $lastError")

    Ok(Json.obj("error" -> false, "msg" -> "movie updated successfully. "  ))
    }


  }





  def findById(id: BSONObjectID) = Action.async {
    val cursor: Future[Cursor[JsObject]] = collection.map {
      _.find(Json.obj("_id" -> id)).cursor[JsObject](ReadPreference.primary)
    }

    // gather all the JsObjects in a list
    val movieList: Future[List[JsObject]] =
      cursor.flatMap(_.collect[List](-1, Cursor.FailOnError[List[JsObject]]()))

    movieList.map {
      movies =>
        if(movies.isEmpty){
          Ok(Json.obj("error" -> true, "msg" -> "Movie not found."))
        } else {
          Ok(Json.obj("error" -> false, "msg" -> "Movie Details", "data" -> movies(0)))
        }
    }
  }





  def remove (id:BSONObjectID) = Action.async {

    collection.flatMap(_.remove(Json.obj("_id"->id))).map {

      result =>

        Ok(Json.obj("error" -> false, "msg" -> "movie details", "data" -> result.toString))

    }

  }


  def updateM(id: BSONObjectID) = Action.async(parse.json) { request =>

    //$pull removes the specified elements from embeded array document
    collection.flatMap(_.update(Json.obj("_id" -> id), Json.obj("$pull" -> Json.obj("movie" -> request.body)))).map(

      lastError =>

        Ok(Json.obj("error" -> true, "msg" -> "movie deleted successfully"))
    )
  }





  def addPhotoMovie(movieId: String) = Action.async(parse.json) { implicit request =>


    request.body.validate[Photo].map { movie =>

      collection.flatMap {
        _.update(
          Json.obj("_id" -> movieId),
          Json.obj("$addToSet" -> Json.obj("photos" -> movie))
        )
      }.map {
        _ =>
          Ok("successfully added")
      }

    }.getOrElse(Future.successful(BadRequest(Json.obj("error" -> true, "msg" -> "Invalid JSON"))))
  }


  def removePhotoM(movieId: String, photoid: String) = Action.async { request =>

    //Logger.debug(s"inside remove api:$request")


    collection.flatMap {
      _.update(
        Json.obj("_id" -> movieId),
        Json.obj("$pull" -> Json.obj("photos" -> Json.obj("photoid" -> photoid)))
      )
    }
      .map { lastError =>

        Ok(Json.obj("error" -> true, "msg" -> "photo deleted successfully"))
      }

  }


  def addLinkMovie(movieId: String) = Action.async(parse.json) { implicit request =>


    request.body.validate[String].map { movie =>
      //request.body.validate[Array[String]].map { movie =>

      collection.flatMap {
        _.update(
          Json.obj("_id" -> movieId),
          //Json.obj("$addToSet" -> Json.obj("movieUrls" -> request.body))
          Json.obj("$addToSet" -> Json.obj("movieUrls" -> movie))

        )
      }.map {
        _ =>
          Ok("successfully added")
      }
    }.getOrElse(Future.successful(BadRequest(Json.obj("error" -> true, "msg" -> "Invalid Json"))))
  }



  /* def removeLinkMovie(movieId: String) = Action.async { implicit request =>


     // request.body.validate[String].map { movie =>
     //request.body.validate[Array[String].map { movie =>


     collection.map {
       // Logger.debug("-----"+request.body.asText.get+"----")
       // Logger.debug("-----"+movieId+"----")
       _.update(
         Json.obj("_id" -> new String(movieId)),
         Json.obj("movieUrls" -> request.body.asText.get)

       ) //.map(_.Array[String])
     }


       .map {
         x =>
           Logger.debug(x.toString)
           Ok(Json.obj("error" -> true, "msg" -> "successfully deleted"))
       }

   }
 */
  //}//.getOrElse(Future.successful(BadRequest(Json.obj("error" -> true, "msg" -> "Invalid Json"))))


  /*
    _match {

      case Some(x) => Ok(Json.obj("error" -> true, "msg" -> "No links found.", "data" -> Json.toJson(x)))

      case None =>
    }

    //collection: Future[JSONCollection]):Future [Option[Movie]]
  }*/



  def removeLink(movieId: String,movieUrls:String) = Action.async { request =>

    //$pull removes the specified elements from embeded array document
    collection.flatMap(_.update(Json.obj("_id" -> movieId),
      Json.obj("$pull" -> Json.obj( "movieUrls" -> movieUrls)))).map(


      lastError =>

        Ok(Json.obj("error" -> true, "msg" -> "Url deleted successfully"))
    )
  }



}
