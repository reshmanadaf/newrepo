package controllers


import javax.inject.Inject
import java.nio.file.Paths

import akka.http.scaladsl.model.HttpCharsetRange.*
import akka.http.scaladsl.model.Uri.Path
import javax.inject._
import model.{Person, Qualification}
import play.api.Logger
import play.api.libs.json
import play.api.libs.json.{JsObject, JsValue, Json}
import play.api.mvc.{AbstractController, ControllerComponents}
import play.modules.reactivemongo.{MongoController, ReactiveMongoApi, ReactiveMongoComponents}
import reactivemongo.api.{Cursor, QueryOpts, ReadPreference}
import reactivemongo.bson.BSONObjectID
import reactivemongo.play.json.collection.JSONCollection
import reactivemongo.play.json._
import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.api.gridfs.ReadFile
import reactivemongo.bson._
import views.html.helper.options

import scala.concurrent.{ExecutionContext, Future}

import  scala.reflect.io.Path
@Singleton
class QualificationController @Inject()(cc: ControllerComponents, val reactiveMongoApi: ReactiveMongoApi)
  extends AbstractController(cc) with MongoController with ReactiveMongoComponents {

  implicit def ec: ExecutionContext = cc.executionContext

  def collection: Future[JSONCollection] = database.map(
    _.collection[JSONCollection]("Qualification"))


  /*
  def create = Action.async {
    val qualification = Qualification("5bc82a2d0748a34ab1c128d0","6th", "BA", "anu","Mangalore")


    //inserting user
    val futureResult = collection.flatMap(_.insert(qualification))

    futureResult.map(_ => Ok)

  }*/


 // val gridfs = reactiveMongoApi.asyncGridFS


  def add() = Action.async(parse.json) { implicit request =>


    request.body.validate[Qualification].map { qualification =>

      collection.flatMap(_.insert(qualification)

      ) .map { lastError =>
        Logger.debug(s"Successfully inserted with LastError: $lastError")
        Created(Json.obj("error" -> false, "msg" -> "Qualification Created."))
      }

    }.getOrElse(Future(BadRequest("invalid json")))

  }


  /*

def update = Action.async(parse.json) { request =>
  val  selector = Json.obj("id" -> id)
  val  modifier = Json.obj ("$set" ->request.body)
collection.flatMap(_.update(selector,modifier,upsert= false,multi= true)).map(

 lastError=>
   Ok(Json.obj("error" ->false,"msg" ->"qualification updated successfully"))
)
}
*/


  def update(id: BSONObjectID) = Action.async(parse.json) { implicit request =>

    //$addtoset operator ensures that there are no duplicate items added to set and does not affect existing duplicate elements
    val updateResult = collection.flatMap(_.update(Json.obj("_id" -> id), Json.obj("$addToSet" -> Json.obj("qualification" -> request.body))))

    updateResult.map { lastError =>
      Ok(Json.obj("error" -> false, "msg" -> "qualification updated successfully."))
    }
  }


  def updateq(id: BSONObjectID) = Action.async(parse.json) { request =>

    //$pull removes the specified elements from embeded array document
    collection.flatMap(_.update(Json.obj("_id" -> id), Json.obj("$pull" -> Json.obj("qualification" -> request.body)))).map(

      lastError =>

        Ok(Json.obj("error" -> true, "msg" -> "qualification deleted successfully"))
    )
  }


  def getQual(id: BSONObjectID) = Action.async { implicit request =>

//finding qualification list with projection operator,to get limits  content of array,while finding document.
    collection.map {
      _.find(Json.obj("_id" -> id), Json.obj("qualification" -> 1)).cursor[JsObject](ReadPreference.primary)
    }.flatMap(_.collect[List](-1, Cursor.FailOnError[List[JsObject]]())).map {


      list =>
        if (list.isEmpty) {
          Ok(Json.obj("error" -> true, "msg" -> "No qualifiaction found."))
        } else {
          Ok(Json.obj("error" -> false, "msg" -> "qualification list", "data" -> (list(0) \ "qualification").get))
          //to get list of qualifictaion details
        }


    }
  }

  def getAll(page: Int, pageSize: Int) = Action.async { implicit request =>

    val skipN = (page - 1) * pageSize
    val QueryOpts = new QueryOpts(skipN = skipN, batchSizeN = pageSize, flagsN = 0)
    collection.map {
      _.find(Json.obj()).options(QueryOpts).cursor[JsObject](ReadPreference.primary)
    }.flatMap(_.collect[List](pageSize, Cursor.FailOnError[List[JsObject]]()).map {
      list =>
        Ok(Json.obj("error" -> false, "msg" -> "qualification list limit ", "data" -> list))
    })

  }




  /*def upload = Action(parse.temporaryFile) { request =>

    request.body.moveTo(Paths.get(s"/home/reshma/Downloaded"), replace = true)
    Ok(Json.obj("msg" -> "file uploaded"))
  }
*/






}