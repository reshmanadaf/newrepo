package controllers




import javax.inject._
import model.JsonFormats._
import model.Person
import play.api.Logger
import play.api.libs.json
import play.api.libs.json.{JsObject, Json}
import play.api.mvc.{AbstractController, ControllerComponents}
import play.modules.reactivemongo.{MongoController, ReactiveMongoApi, ReactiveMongoComponents}
import reactivemongo.api.{Cursor, ReadPreference}
import reactivemongo.bson.BSONObjectID
import reactivemongo.play.json.collection.JSONCollection
import reactivemongo.play.json._

import scala.concurrent.{ExecutionContext, Future}



/**
 * This controller creates an `Action` that demonstrates how to write
 * simple asynchronous code in a controller. It uses a timer to
 * asynchronously delay sending a response for 1 second.
 *
 * @param cc standard controller components
 * @param actorSystem We need the `ActorSystem`'s `Scheduler` to
 * run code after a delay.
 * @param exec We need an `ExecutionContext` to execute our
 * asynchronous code.  When rendering content, you should use Play's
 * default execution context, which is dependency injected.  If you are
 * using blocking operations, such as database or network access, then you should
 * use a different custom execution context that has a thread pool configured for
 * a blocking API.
 */
@Singleton
class AsyncController @Inject() (cc: ControllerComponents, val reactiveMongoApi: ReactiveMongoApi)
  extends AbstractController(cc) with MongoController with ReactiveMongoComponents {

  implicit def ec: ExecutionContext = cc.executionContext


  def collection: Future[JSONCollection] = database.map(
    _.collection[JSONCollection]("Person"))


 /* def create = Action.async {
    val person = Person("naina", "billa", 40)


    //inserting user
    val futureResult = collection.flatMap(_.insert(person))

    //val nameUpperResult: JsResult[String] = nameResult.map(_.toUpperCase())

    futureResult.map(_ => Ok)

  }*/

  def createFromJson(id: String) = Action.async(parse.json) { implicit request =>

  /*val p = request.body.as[JsObject] + ("_id" -> Json.toJson(BSONObjectID.generate.stringify())
  p.validate[Person].map { person =>

    val inResult = Person.add(collection,person)
    inResult.map{ lastError =>
     Logger.debug(s"successfully inserted with lasterror: $lastError")
      Created
    }
  }.getOrElse(Future.successful(BadRequest("invalid json"))

  }*/


    request.body.validate[Person].map { person =>
      collection.flatMap(_.insert(person)).map { lastError =>
        Logger.debug(s"Successfully inserted with LastError: $lastError")
        Created(Json.obj("error" -> false, "msg" -> "Person Created."))
      }

    }.getOrElse(Future.successful(BadRequest("invalid json")))

    //Ok("Got Request ["+request+ "]")
  }

  def findByName(firstname: String) = Action.async {

    val cursor: Future[Cursor[Person]] = collection.map {

      _.find(Json.obj("firstname" -> firstname)).

        cursor[Person]()
    }
    val futureUserList: Future[List[Person]] =
      cursor.flatMap(_.collect[List](-1, Cursor.FailOnError[List[Person]]()))

    futureUserList.map { person =>
      Ok(person.toString)
    }
  }


  def update = Action.async(parse.json) { request =>

    val selector = Json.obj("firstname" -> "dada1")


    val modifier = Json.obj("$set" -> request.body)


    collection.flatMap(_.update(selector, modifier, upsert = false, multi = true)).map(
      lastError =>

        //Person
        //Ok(person.toString)
      Ok(Json.obj("error" -> false, "msg" -> "User updated successfully."))
    )

  }


  /* def remove = Action.async(parse.json) { request =>
    val selector1 = request.body

    collection.flatMap(_.remove(selector1))


  }
*/

  def remove = Action.async(parse.json) { request =>

    collection.flatMap(_.remove(request.body.as[JsObject] )).map(
      lastError =>

    //Ok(s"Prop is :$selector1")
    Ok(Json.obj("error" -> false, "msg" -> "Person deleted successfully."))

    )

    //Logger.warn("no body found")
    //NotFound("no json body found")


    // selector1.validate[person].map{  Person =>


   // collection.flatMap(_.remove(selector1)(Json.obj("_id" -> objectId)))

    // val futureString = Future[String] = scala.concurrent.Future{

    //val selector1 = request.body

    /* val futureString = scala.concurrent.Future {
      collection.flatMap(_.remove(true)(JsObject("id" -> id)))
    }
    futureString.map(remove => Ok("Got result: " + remove)) }*/








  }
}


    /* def delete(id:String)= Action {

    val objectId=JsObject.(parse.json)(id)
    collection.flatMap(JsObject("_id" -> objectId.get))
    Redirect(routes.AsyncController.index)
  }
*/


/* def createFromJson = Action  { request =>
    val json = request.body.asJson.get()
    val fromJson = json.as[createFromJson]
    println(fromJson)
    Ok
  }

*/

/*  def CreatePerson(person : Person) : Future[Unit] =

    collection.flatMap(_.insert(person) map(_=> {} ) ) {


      def updatePerson(person: Person)Future [Int] = {
      val selector = Json.obj(
        "firstname" -> person.firstname,
        "lastname" -> person.lastname
      )

        collection.flatMap(_.update(selector,person).map(_.n))
      }
    }*/
